﻿using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Challenge1
{
    public static class DBConnection
    {
        public static event Action? StatusChanged;

        public static SqlConnection sqlConnection;

        private static bool isConnected;

        static DBConnection()
        {
            sqlConnection = new SqlConnection();
            isConnected = false;
        }
        public static void OpenConnection(string dataSource, string initialCatalog, bool integratedSecurity, string userID, string password)
        {
            SqlConnectionStringBuilder strCon = new SqlConnectionStringBuilder()
            {
                DataSource = dataSource,
                InitialCatalog = initialCatalog,
                IntegratedSecurity = integratedSecurity,
                UserID = userID,
                Password = password
            };

            sqlConnection.ConnectionString = strCon.ConnectionString;

            sqlConnection.StateChange += (s, e) => {
                if((s as SqlConnection).State == ConnectionState.Open) isConnected = true;
                else isConnected = false;
                StatusChanged?.Invoke();
            };

            try
            {
                sqlConnection.Open();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public static bool CheckIsConnected()
        {
            return isConnected;
        }

        public static void PrepareDB(SqlDataAdapter dataAdapter)
        {
            var sql = @"SELECT * FROM [Table] Order By [Table].Id";
            dataAdapter.SelectCommand = new SqlCommand(sql, DBConnection.sqlConnection);

            sql = @"INSERT INTO [Table] (LastName, FirstName, SurName, PhoneNumber, Email) 
                        VALUES (@lastName, @firstName, @surname, @phoneNumber, @email)
                        SET @id=@@IDENTITY;";
            dataAdapter.InsertCommand = new SqlCommand(sql, DBConnection.sqlConnection);
            dataAdapter.InsertCommand.Parameters.Add("@id", SqlDbType.Int, 4, "Id").Direction = ParameterDirection.Output;
            dataAdapter.InsertCommand.Parameters.Add("@lastName", SqlDbType.NVarChar, 50, "LastName");
            dataAdapter.InsertCommand.Parameters.Add("@firstName", SqlDbType.NVarChar, 50, "FirstName");
            dataAdapter.InsertCommand.Parameters.Add("@surname", SqlDbType.NVarChar, 50, "SurName");
            dataAdapter.InsertCommand.Parameters.Add("@phoneNumber", SqlDbType.NVarChar, 20, "PhoneNumber");
            dataAdapter.InsertCommand.Parameters.Add("@email", SqlDbType.NVarChar, 50, "Email");

            sql = @"UPDATE [Table] SET
                    LastName = @lastName, FirstName = @firstName, SurName = @surname,
                    PhoneNumber = @phoneNumber, Email = @email
                    WHERE Id = @id";
            dataAdapter.UpdateCommand = new SqlCommand(sql, DBConnection.sqlConnection);
            dataAdapter.UpdateCommand.Parameters.Add("@id", SqlDbType.Int, 4, "Id").SourceVersion = DataRowVersion.Original;
            dataAdapter.UpdateCommand.Parameters.Add("@lastName", SqlDbType.NVarChar, 50, "LastName");
            dataAdapter.UpdateCommand.Parameters.Add("@firstName", SqlDbType.NVarChar, 50, "FirstName");
            dataAdapter.UpdateCommand.Parameters.Add("@surname", SqlDbType.NVarChar, 50, "SurName");
            dataAdapter.UpdateCommand.Parameters.Add("@phoneNumber", SqlDbType.NVarChar, 20, "PhoneNumber");
            dataAdapter.UpdateCommand.Parameters.Add("@email", SqlDbType.NVarChar, 50, "Email");

            sql = @"DELETE FROM [Table] WHERE Id = @id";
            dataAdapter.DeleteCommand = new SqlCommand(sql, DBConnection.sqlConnection);
            dataAdapter.DeleteCommand.Parameters.Add("@id", SqlDbType.Int, 4, "Id");
        }

    }
}
