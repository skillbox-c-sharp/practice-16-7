﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Data.SqlClient;

namespace Challenge1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        SqlDataAdapter dataAdapter;
        DataTable dt;
        OleDbDataAdapter oleDataAdapter;
        DataTable oleDt;
        List<OnlineOrder> orders;
        List<Customer> customers;

        public MainWindow()
        {
            InitializeComponent();
            DBConnection.StatusChanged += ChangeStatusLable;
            DBOleConnection.StatusChanged += ChangeStatusLable;
        }

        private void Prepair()
        {
            dt = new DataTable();
            dataAdapter = new SqlDataAdapter();
            oleDt = new DataTable();
            oleDataAdapter = new OleDbDataAdapter();
            orders = new List<OnlineOrder>();
            customers = new List<Customer>();
            PrepareDBs();
            RefreshData();
            dataGrid.ItemsSource = orders;
        }

        private void RefreshData()
        {
            ClearLists();
            ClearDts();
            FillDts();
            FillCustomers();
            FillOrders();
            dataGrid.Items.Refresh();
        }

        private void PrepareDBs()
        {
            DBConnection.PrepareDB(dataAdapter);
            DBOleConnection.PrepareDB(oleDataAdapter);
        }

        private void ClearLists()
        {
            customers.Clear();
            orders.Clear();
        }

        private void ClearDts()
        {
            oleDt.Clear();
            dt.Clear();
        }

        private void FillDts()
        {
            dataAdapter.Fill(dt);
            oleDataAdapter.Fill(oleDt);
        }

        private void FillCustomers()
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                customers.Add(new Customer((int)dt.Rows[i]["Id"], dt.Rows[i]["FirstName"].ToString(),
                    dt.Rows[i]["LastName"].ToString(), dt.Rows[i]["SurName"].ToString(),
                    dt.Rows[i]["PhoneNumber"].ToString(), dt.Rows[i]["Email"].ToString()));
            }
        }

        private void FillOrders()
        {
            for (int i = 0; i < oleDt.Rows.Count; i++)
            {
                DataRow[] dr = dt.Select($"Email = '{oleDt.Rows[i]["Email"]}'");
                orders.Add(new OnlineOrder((int)oleDt.Rows[i]["Id"], dr[0]["FirstName"].ToString(),
                    dr[0]["LastName"].ToString(), dr[0]["SurName"].ToString(), dr[0]["PhoneNumber"].ToString(),
                    oleDt.Rows[i]["Email"].ToString(), (int)oleDt.Rows[i]["ProductCode"], oleDt.Rows[i]["ProductName"].ToString()));
            }
        }

        private void openDialogLoginButton_Click(object sender, RoutedEventArgs e)
        {
            LoginWindow loginWindow = new LoginWindow();
            loginWindow.ShowDialog();
            if (loginWindow.DialogResult.Value)
            {
                Prepair();
            }
        }

        private void ChangeStatusLable()
        {
            if (DBOleConnection.CheckIsConnected() && DBConnection.CheckIsConnected())
            {
                statusLabel.Dispatcher.Invoke(() =>
                {
                    statusLabel.Content = "Статус соединения: Соединение установлено";
                    openDialogLoginButton.Visibility = Visibility.Hidden;
                });
            }
            else
            {
                statusLabel.Dispatcher.Invoke(() =>
                {
                    statusLabel.Content = "Статус соединения: Соединение не установлено";
                    openDialogLoginButton.Visibility = Visibility.Visible;
                });
            }
        }

        private void MenuItemAddOrderClick(object sender, RoutedEventArgs e)
        {

            DataRow dr = oleDt.NewRow();

            AddOnlineOrderWindow addOrderWindow = new AddOnlineOrderWindow(customers, dr);
            addOrderWindow.ShowDialog();

            if (addOrderWindow.DialogResult.Value)
            {
                oleDt.Rows.Add(dr);
                oleDataAdapter.Update(oleDt);
                RefreshData();
            }
        }

        private void MenuItemAddCustomerClick(object sender, RoutedEventArgs e)
        {
            DataRow dr = dt.NewRow();

            AddCustomerWindow addCustomerWindow = new AddCustomerWindow(dr);
            addCustomerWindow.ShowDialog();

            if (addCustomerWindow.DialogResult.Value)
            {
                dt.Rows.Add(dr);
                dataAdapter.Update(dt);
                RefreshData();
            }
        }

        private void MenuItemDeleteOrderClick(object sender, RoutedEventArgs e)
        {
            oleDt.Rows[dataGrid.SelectedIndex].Delete();
            oleDataAdapter.Update(oleDt);
            RefreshData();
        }

        private void dataGrid_CurrentCellChanged(object sender, EventArgs e)
        {
            if (dataGrid.SelectedIndex == -1) return;

            dataGrid.CommitEdit();
            bool isChanged = false;
            string email = ((OnlineOrder)dataGrid.SelectedItem).Email;
            string firstName = ((OnlineOrder)dataGrid.SelectedItem).FirstName;
            string lastName = ((OnlineOrder)dataGrid.SelectedItem).LastName;
            string surName = ((OnlineOrder)dataGrid.SelectedItem).SurName;
            string phoneNumber = ((OnlineOrder)dataGrid.SelectedItem).PhoneNumber;

            DataRow[] dr = dt.Select($"Email = '{email}'");

            if(!dr[0]["FirstName"].Equals(firstName) || !dr[0]["LastName"].Equals(lastName)
                || !dr[0]["SurName"].Equals(surName) || !dr[0]["PhoneNumber"].Equals(phoneNumber))
            {
                isChanged = true;
                if (!string.IsNullOrEmpty(firstName)) dr[0]["FirstName"] = firstName;
                if (!string.IsNullOrEmpty(lastName)) dr[0]["LastName"] = lastName;
                if (!string.IsNullOrEmpty(surName)) dr[0]["SurName"] = surName;
                bool isPhoneNumber = Regex.IsMatch(phoneNumber, @"^(\+[0-9]{11})$");
                if (isPhoneNumber || string.IsNullOrEmpty(phoneNumber)) dr[0]["PhoneNumber"] = phoneNumber;
                else MessageBox.Show("Неккоректный номер телефона");
            }
            dt.Rows[dt.Rows.IndexOf(dr[0])].EndEdit();
            dataAdapter.Update(dt);
            if(isChanged) RefreshData();
        }

        private void dataGrid_CellEditEnding(object sender, DataGridCellEditEndingEventArgs e)
        {
            string email = ((OnlineOrder)dataGrid.SelectedItem).Email;
            DataRow[] dr = dt.Select($"Email = '{email}'");
            dt.Rows[dt.Rows.IndexOf(dr[0])].BeginEdit();
            dataGrid.BeginEdit();
        }
    }
}
