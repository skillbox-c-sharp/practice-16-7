﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge1
{
    class OnlineOrder
    {
        public int Id { get; private set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string SurName { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public int ProductCode { get; set; }
        public string ProductName { get; set; }

        public OnlineOrder(int id, string firstName, string lastName, string surName, 
            string phoneNumber, string email, int productCode, string productName)
        {
            Id = id;
            FirstName = firstName;
            LastName = lastName;
            SurName = surName;
            PhoneNumber = phoneNumber;
            Email = email;
            ProductCode = productCode;
            ProductName = productName;
        }

    }
}
