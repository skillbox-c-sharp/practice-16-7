﻿using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Challenge1
{
    public static class DBOleConnection
    {
        public static event Action? StatusChanged;

        public static OleDbConnection oleConnection;

        private static bool isConnected;

        static DBOleConnection()
        {
            oleConnection = new OleDbConnection();
            isConnected = false;
        }

        public static void OpenConnection(string dataSource, string provider)
        {
            OleDbConnectionStringBuilder strOleCon = new OleDbConnectionStringBuilder()
            {
                DataSource = dataSource,
                Provider = provider
            };

            oleConnection.ConnectionString = strOleCon.ConnectionString;

            oleConnection.StateChange += (s, e) => {
                if ((s as OleDbConnection).State == ConnectionState.Open) isConnected = true;
                else isConnected = false;
                StatusChanged?.Invoke();
            };

            try
            {
                oleConnection.Open();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public static bool CheckIsConnected()
        {
            return isConnected;
        }

        public static void PrepareDB(OleDbDataAdapter oleDataAdapter)
        {
            var ole = @"SELECT * FROM [SecondTable] Order By [SecondTable].Id";
            oleDataAdapter.SelectCommand = new OleDbCommand(ole, DBOleConnection.oleConnection);

            ole = @"INSERT INTO [SecondTable] (Email, ProductCode, ProductName) 
                        VALUES (@email, @productCode, @productName)";
            oleDataAdapter.InsertCommand = new OleDbCommand(ole, DBOleConnection.oleConnection);
            oleDataAdapter.InsertCommand.Parameters.Add("@email", OleDbType.WChar, 50, "Email");
            oleDataAdapter.InsertCommand.Parameters.Add("@productCode", OleDbType.Integer, 4, "ProductCode");
            oleDataAdapter.InsertCommand.Parameters.Add("@productName", OleDbType.WChar, 50, "ProductName");

            ole = @"DELETE FROM [SecondTable] WHERE Id = @id";
            oleDataAdapter.DeleteCommand = new OleDbCommand(ole, DBOleConnection.oleConnection);
            oleDataAdapter.DeleteCommand.Parameters.Add("@id", OleDbType.Integer, 4, "Id");
        }
    }
}
