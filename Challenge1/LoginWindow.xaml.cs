﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Challenge1
{
    /// <summary>
    /// Логика взаимодействия для LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : Window
    {
        public LoginWindow()
        {
            InitializeComponent();
            cancelButton.Click += delegate { DialogResult = false; };
        }


        private async void loginButton_Click(object sender, RoutedEventArgs e)
        {
            string login = loginTextBox.Text;
            string password = passwordTextBox.Text;
            if(!(string.IsNullOrEmpty(login) || string.IsNullOrEmpty(password)))
            {
                bool isConnected = await OpenConnectionDBs(login, password);
                if (isConnected) DialogResult = true;
            }
            else
            {
                MessageBox.Show("Введите логин и пароль для входа");
            }
        }

        private async Task<bool> OpenConnectionDBs(string login, string password)
        {
            await Task.Run(() =>
            {
                DBConnection.OpenConnection(@"(localdb)\MSSQLLocalDB", "MSSQLPractice", false, login, password);
            });
            if (DBConnection.CheckIsConnected())
            {
                await Task.Run(() =>
                    {
                        DBOleConnection.OpenConnection(@"SecondDatabase.accdb", @"Microsoft.ACE.OLEDB.12.0"); 
                    });
                if (DBConnection.CheckIsConnected())
                {
                    return true;
                }
            }
            return false;
        }
    }
}
